package com.xcallibur.arget.animamastertoolkit.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xcallibur.arget.animamastertoolkit.utils.AttributeDifficulties

class DashboardViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = AttributeDifficulties.values().toString()
    }
    val text: LiveData<String> = _text
}