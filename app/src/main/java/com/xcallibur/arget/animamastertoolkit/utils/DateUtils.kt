package com.xcallibur.arget.animamastertoolkit.utils

import com.xcallibur.arget.animamastertoolkit.utils.TimeUnits.*
import kotlin.math.floor
import kotlin.math.roundToInt


enum class TimeUnits(val toSeconds: Double) {
    SECOND(1.0),
    TURN(3.0),
    MINUTE(60.0),
    HOUR(3600.0),
    DAY(86400.0),
    MONTH(2592000.0)
}


fun fromMinutesToReadableString(minutes: Double): String {

    val seconds = minutes * MINUTE.toSeconds

    return fromSecondsToReadableString(seconds)
}


fun fromSecondsToReadableString(seconds: Double): String {

    val quantity: Int
    when (seconds) {

        in MONTH.toSeconds..Double.MAX_VALUE -> {

            quantity = floor(seconds / MONTH.toSeconds).roundToInt()
            return "$quantity Months, " +
                    fromSecondsToReadableString(seconds - MONTH.toSeconds * quantity)
        }

        in DAY.toSeconds..MONTH.toSeconds -> {

            quantity = floor(seconds / DAY.toSeconds).roundToInt()
            return "$quantity Days, " +
                    fromSecondsToReadableString(seconds - DAY.toSeconds * quantity)
        }

        in HOUR.toSeconds..DAY.toSeconds -> {

            quantity = floor(seconds / HOUR.toSeconds).roundToInt()
            return "$quantity Hours, " +
                    fromSecondsToReadableString(seconds - HOUR.toSeconds * quantity)
        }

        in MINUTE.toSeconds..HOUR.toSeconds -> {
            quantity = floor(seconds / MINUTE.toSeconds).roundToInt()
            return "$quantity Minutes, " +
                    fromSecondsToReadableString(seconds - MINUTE.toSeconds * quantity)
        }

        in TURN.toSeconds..MINUTE.toSeconds -> {
            quantity = floor(seconds / TURN.toSeconds).roundToInt()
            return "$quantity Turns" +
                    fromSecondsToReadableString(seconds - TURN.toSeconds * quantity)
        }

        in SECOND.toSeconds..TURN.toSeconds -> {
            quantity = floor(seconds / SECOND.toSeconds).roundToInt()
            return "$quantity Seconds"
        }
    }

    return ""

}