package com.xcallibur.arget.animamastertoolkit.model

class Result {

    var descriptionRoll = ""
    var descriptionLong = ""

    var value = 0.0
    var extraValue = 0.0

    constructor(description: String, result: Double) {
        this.descriptionRoll = description
        this.value = result
    }

    override fun toString(): String {
        return "Result: Description of roll '$descriptionRoll', Description '$descriptionLong', value=$value, extraValue=$extraValue)"
    }


}