package com.xcallibur.arget.animamastertoolkit.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.xcallibur.arget.animamastertoolkit.R
import com.xcallibur.arget.animamastertoolkit.adapters.CharacterCreationAdapter.Type.*
import com.xcallibur.arget.animamastertoolkit.dataModel.Attribute
import com.xcallibur.arget.animamastertoolkit.model.Character

class CharacterCreationAdapter(
    private val context: Context,
    private val character: Character,
    private val adapterType: Type
) {

    enum class Type {
        SECONDARY, RESISTANCES, COMBAT, ATTRIBUTES
    }

    fun getItemViewType(position: Int): Int {
        when (adapterType) {
            SECONDARY -> return R.layout.support_simple_spinner_dropdown_item
            RESISTANCES -> TODO()
            COMBAT -> TODO()
            ATTRIBUTES -> TODO()
        }

    }

    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        val v = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            viewType, parent, false
        )
        return ViewHolder(v)
    }

    private fun getArrayType(): List<Attribute> {
        when (adapterType) {
            SECONDARY -> return character.skills
            RESISTANCES -> TODO()
            COMBAT -> TODO()
            ATTRIBUTES -> TODO()
        }
    }

    fun getItem(position: Int): Any? {
        return getArrayType()[position]
    }

    fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        if (holder == null) return



        holder.bind(getArrayType()[position])
    }


    fun getItemCount(): Int {
        return getArrayType().size
    }


    class ViewHolder(var itemRowBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(itemRowBinding.root) {
        fun bind(obj: Any?) {
            //  itemRowBinding.setVariable(BR.load, obj)
            itemRowBinding.executePendingBindings()
        }
    }


}