package com.xcallibur.arget.animamastertoolkit

import android.os.Bundle
import android.service.autofill.OnClickAction
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SimpleAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.xcallibur.arget.animamastertoolkit.databinding.ActivityMainBinding
import com.xcallibur.arget.animamastertoolkit.utils.AttributeDifficulties


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        navView.setupWithNavController(navController)

        val colors = arrayOf(
            "Red",
            "Blue",
            "White",
            "Yellow",
            "Black",
            "Green",
            "Purple",
            "Orange",
            "Grey"
        )

        binding.mainActivity = this


    }
}
