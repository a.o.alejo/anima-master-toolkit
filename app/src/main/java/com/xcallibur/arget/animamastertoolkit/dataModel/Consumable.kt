package com.xcallibur.arget.animamastertoolkit.dataModel

import kotlin.math.min

class Consumable {

    var maximumValue = 100.0
    var temporaryValue = 100.0
    var minimumValue = 0.0
    var regenerationRatePerTurn = 10.0

    fun isBelowLimit(): Boolean {
        return temporaryValue < minimumValue && temporaryValue != maximumValue
    }


    fun recover(turns: Double) {
        addAndCheckMax(regenerationRatePerTurn * turns)
    }

    fun addAndCheckMax(value: Double) {
        temporaryValue = min(temporaryValue + value, maximumValue)
    }


}