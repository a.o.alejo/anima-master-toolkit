package com.xcallibur.arget.animamastertoolkit.model

import com.xcallibur.arget.animamastertoolkit.utils.DamageTypes

class Weapon {

    enum class weaponType {
        ONE_HAND,
        TWO_HANDS,
        TROWABLE,
        PROJECTILE,
        SHOT,
        PSYQUIC,
        MAGIC
    }

    enum class weaponSize {
        STANDARD,
        LARGE,
        HUGE
    }

    var name = ""

    var baseDamage = 40.0
    var speed = 10.0
    var type = weaponType.ONE_HAND
    var primaryCritical = DamageTypes.CUT
    var secondaryCritical = DamageTypes.CUT

    var damagesEnergy = false
    var fortitude = 10.0
    var breakage = 10.0
    var prescence = 10.0

    var bonusToAttack = 0.0
    var bonusToStop = 0.0
    var bonusToEvade = 0.0
    var bonusToStrength = 0.0

    var penetrationTA = 1.0

    var range = 0.0
    var cadency = 0.0
    var recharge = 0.0

    var quality = 0.0

    var size = weaponSize.STANDARD

    var projectile = Projectile()

}