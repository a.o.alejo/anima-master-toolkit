package com.xcallibur.arget.animamastertoolkit.model

import com.xcallibur.arget.animamastertoolkit.dataModel.Attribute
import com.xcallibur.arget.animamastertoolkit.dataModel.Attribute.AttributeTypes.*
import com.xcallibur.arget.animamastertoolkit.dataModel.Consumable
import com.xcallibur.arget.animamastertoolkit.model.Roll.RollTypes.D10
import com.xcallibur.arget.animamastertoolkit.model.Roll.RollTypes.D100
import com.xcallibur.arget.animamastertoolkit.utils.*

class Character {

    var playerName = ""
    var characterName = ""
    var description = ""

    var penalizer = Penalizer()

    var characteristics = List(Characteristics.values().size) {
        Attribute(
            6.0,
            CHARACTERISTIC,
            Characteristics.values()[it].name
        )
    }
    var skills = List(Skills.values().size) {
        Attribute(
            -30.0,
            SKILL,
            Skills.values()[it].name
        )
    }
    var resistances = List(Resistances.values().size) {
        Attribute(
            30.0,
            RESISTANCE,
            Resistances.values()[it].name
        )
    }
    var consumables = List(Consumables.values().size) { Consumable() }

    var rollCharacteristic = Roll(D10)
    var rollSkills = Roll(D100)


    fun getMovementVelocityIndex(): Double {
        return characteristics[Characteristics.AGI.ordinal].total() - penalizer.getPenalizer(
            Penalizer.Type.MOVEMENT
        )
    }


    fun rollFor(type: Resistances): Result {

        return rollCharacteristic.roll(resistances[type.ordinal].total(), true)

    }

    fun rollFor(type: Characteristics): Result {

        return rollCharacteristic.roll(characteristics[type.ordinal].total(), false)

    }

    fun rollFor(type: Skills): Result {

        return rollSkills.roll(skills[type.ordinal].total(), false)

    }

    fun clearNegativeStatus() {
        characteristics.filter { it.temporaryValue < 0 }.forEach { it.temporaryValue = 0.0 }
    }

    fun clearPositiveStatus() {
        characteristics.filter { it.temporaryValue > 0 }.forEach { it.temporaryValue = 0.0 }
    }

    fun clearStatus() {
        characteristics.forEach { it.temporaryValue = 0.0 }
    }


}