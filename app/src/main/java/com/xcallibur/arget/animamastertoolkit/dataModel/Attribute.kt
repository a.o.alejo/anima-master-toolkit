package com.xcallibur.arget.animamastertoolkit.dataModel

import kotlin.math.roundToInt


class Attribute(var baseValue: Double, var attributeType: AttributeTypes, val name: String) {

    var additionalValue = 0.0
    var temporaryValue = 0.0


    enum class AttributeTypes {
        CHARACTERISTIC,
        SKILL,
        RESISTANCE
    }


    fun total(): Double {
        return baseValue + additionalValue + temporaryValue
    }

    fun calculateModifier(): Double {

        return (-99.99539380164042 + total() * (-89.73749436796427 +
                total() * 20.86274492873181)) /
                (1.0 + total() * (4.153652379694570 + total() * (0.3999242127526147 +
                        total() * -0.01281322967405615)))
    }

    fun calculateModifierOld(): Double {
        val tableModifiers = arrayListOf(
            -100.0, -30.0, -20.0, -10.0, -5.0, 0.0, 5.0, 5.0, 10.0, 10.0,
            15.0, 20.0, 20.0, 25.0, 25.0, 30.0, 35.0, 35.0, 40.0, 40.0, 45.0
        )

        try {
            return tableModifiers[total().roundToInt()]

        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
        }

        return 0.0
    }

    override fun toString(): String {
        return "Attribute(baseValue=$baseValue, attributeType=$attributeType, additionalValue=$additionalValue, temporaryValue=$temporaryValue)\n"
    }


}
