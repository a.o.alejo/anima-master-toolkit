package com.xcallibur.arget.animamastertoolkit.utils

import com.xcallibur.arget.animamastertoolkit.R
import com.xcallibur.arget.animamastertoolkit.utils.SkillsCategories.*
import com.xcallibur.arget.animamastertoolkit.utils.Zones.*


enum class DamageTypes(nameResource: Int) {
    CUT(R.string.cut),
    IMPACT(R.string.impact),
    THRUST(R.string.thrust),
    HEAT(R.string.heat),
    COLD(R.string.cold),
    ELECTRICITY(R.string.electricity),
    ENERGY(R.string.energy)
}

enum class Characteristics(nameResource: Int) {
    STR(R.string.strength),
    DEX(R.string.dexterity),
    AGI(R.string.agility),
    CON(R.string.constitution),
    POW(R.string.power),
    INT(R.string.intelligence),
    WP(R.string.willpower),
    PER(R.string.perception)
}

enum class Resistances(nameResource: Int) {
    PHYSICAL(R.string.physical),
    DISEASE(R.string.disease),
    POISON(R.string.poison),
    MAGIC(R.string.magic),
    PSYCHIC(R.string.psychic),
    PRESENCE(R.string.presence)
}

enum class Zones(nameResource: Int) {
    HEAD(R.string.head),
    CHEST(R.string.chest),
    ARMS(R.string.arms),
    LEGS(R.string.legs),
    HANDS(R.string.hands),
    NOT_DEFINED(R.string.notDefined)
}

enum class Locations(val zone: Zones) {
    PECTORAL_L(CHEST),
    PECTORAL_R(CHEST),
    SHOULDER_L(CHEST),
    SHOULDER_R(CHEST),
    STOMACH_C(CHEST),
    STOMACH_L(CHEST),
    STOMACH_R(CHEST),
    FOREARM_SUP_L(ARMS),
    FOREARM_SUP_R(ARMS),
    FOREARM_INF_L(ARMS),
    FOREARM_INF_R(ARMS),
    HAND_L(HANDS),
    HAND_R(HANDS),
    THIGH_L(LEGS),
    CALF_L(LEGS),
    FOOT_L(LEGS),
    THIGH_R(LEGS),
    CALF_R(LEGS),
    FOOT_R(LEGS),
    HEAD(Zones.HEAD),
    NECK(CHEST)
}


enum class Consumables(nameResource: Int) {
    HIT_POINTS(R.string.hitPoints),
    ZEON(R.string.zeon),
    SHIELD(R.string.shield),
    ENERGY(R.string.energy),
    MENTAL_HEALTH(R.string.mentalHealth)
}

enum class SkillsCategories(nameResource: Int) {
    COMBAT(R.string.combat),
    ATHLETICS(R.string.athletics),
    VIGOR(R.string.vigor),
    PERCEPTION(R.string.perceptive),
    INTELLECTUAL(R.string.intellectual),
    SOCIAL(R.string.social),
    SUBTERFUGE(R.string.subterfuge),
    CREATIVE(R.string.creative),
    SPECIAL(R.string.special)
}


enum class Skills(val category: SkillsCategories, nameResource: Int) {
    ATTACK(COMBAT, R.string.attack),
    DODGE(COMBAT, R.string.dodge),
    BLOCK(COMBAT, R.string.block),
    MAGIC_PROJECTION_DEFENSIVE(COMBAT, R.string.magicProjectionDefensive),
    MAGIC_PROJECTION_OFFENSIVE(COMBAT, R.string.magicProjectionOffensive),
    PSYCHIC_PROJECTION_DEFENSIVE(COMBAT, R.string.psychicProjectionDefensive),
    PSYCHIC_PROJECTION_OFFENSIVE(COMBAT, R.string.psychicProjectionOffensive),

    ACROBATICS(ATHLETICS, R.string.acrobatics),
    ATHLETICISM(ATHLETICS, R.string.athleticism),
    CLIMB(ATHLETICS, R.string.climb),
    JUMP(ATHLETICS, R.string.jump),
    RIDE(ATHLETICS, R.string.ride),
    SWIM(ATHLETICS, R.string.swim),
    PILOT(ATHLETICS, R.string.pilot),

    COMPOSURE(VIGOR, R.string.composure),
    FEATS_OF_STRENGTH(VIGOR, R.string.feats_of_strength),
    WITHSTAND_PAIN(VIGOR, R.string.withstand_pain),

    NOTICE(PERCEPTION, R.string.notice),
    SEARCH(PERCEPTION, R.string.search),
    TRACK(PERCEPTION, R.string.track),

    ANIMALS(INTELLECTUAL, R.string.animals),
    APPRAISAL(INTELLECTUAL, R.string.appraisal),
    HERBAL_LORE(INTELLECTUAL, R.string.herbal_lore),
    HISTORY(INTELLECTUAL, R.string.history),
    MAGIC_APPRAISAL(INTELLECTUAL, R.string.magic_appraisal),
    MEDICINE(INTELLECTUAL, R.string.medicine),
    MEMORIZE(INTELLECTUAL, R.string.memorize),
    NAVIGATION(INTELLECTUAL, R.string.navigation),
    OCCULT(INTELLECTUAL, R.string.occult),
    SCIENCES(INTELLECTUAL, R.string.sciences),
    TACTICS(INTELLECTUAL, R.string.tactics),
    LAW(INTELLECTUAL, R.string.law),

    INTIMIDATE(SOCIAL, R.string.intimidate),
    LEADERSHIP(SOCIAL, R.string.leadership),
    PERSUASION(SOCIAL, R.string.persuasion),
    STYLE(SOCIAL, R.string.style),
    COMMERCE(SOCIAL, R.string.commerce),
    ETIQUETTE(SOCIAL, R.string.etiquette),
    STREETWISE(SOCIAL, R.string.streetwise),

    DISGUISE(SUBTERFUGE, R.string.disguise),
    HIDE(SUBTERFUGE, R.string.hide),
    LOCK_PICKING(SUBTERFUGE, R.string.lock_picking),
    POISONS(SUBTERFUGE, R.string.poisons),
    THEFT(SUBTERFUGE, R.string.theft),
    TRAP_LORE(SUBTERFUGE, R.string.trap_lore),
    STEALTH(SUBTERFUGE, R.string.stealth),

    ART(CREATIVE, R.string.art),
    DANCE(CREATIVE, R.string.dance),
    FORGING(CREATIVE, R.string.forging),
    MUSIC(CREATIVE, R.string.music),
    SLEIGHT_OF_HAND(CREATIVE, R.string.sleight_of_hand),
    RUNES(CREATIVE, R.string.runes),
    ALCHEMY(CREATIVE, R.string.alchemy),
    CALLIGRAPHY(CREATIVE, R.string.calligraphy),
    GOLDSMITH(CREATIVE, R.string.goldsmith),
    CONFECTION(CREATIVE, R.string.confection),
    ANIMISM(CREATIVE, R.string.animism),
    PUPPETS_MAKING(CREATIVE, R.string.puppets_making),

    SPECIAL_1(SPECIAL, R.string.special),
    SPECIAL_2(SPECIAL, R.string.special),
    SPECIAL_3(SPECIAL, R.string.special),
    SPECIAL_4(SPECIAL, R.string.special),
    SPECIAL_5(SPECIAL, R.string.special),
    SPECIAL_6(SPECIAL, R.string.special),
    SPECIAL_7(SPECIAL, R.string.special),
    SPECIAL_8(SPECIAL, R.string.special),
    SPECIAL_9(SPECIAL, R.string.special)
}


enum class SkillDifficulties(
    val range: ClosedFloatingPointRange<Double>
    , val nameResource: Int
) {
    ZEN(440.0..Double.MAX_VALUE, R.string.zen),
    INHUMAN(320.0..440.0, R.string.inhuman),
    IMPOSSIBLE(280.0..320.0, R.string.impossible),
    ALMOST_IMPOSSIBLE(240.0..280.0, R.string.almostImpossible),
    ABSURD(180.0..240.0, R.string.absurd),
    VERY_DIFFICULT(140.0..180.0, R.string.veryDifficult),
    DIFFICULT(120.0..140.0, R.string.difficult),
    MODERATE(80.0..120.0, R.string.moderate),
    EASY(40.0..80.0, R.string.easy),
    ROUTINE(20.0..40.0, R.string.routine),
    DISASTER(-Double.MAX_VALUE..20.0, R.string.disaster),
    NOT_DEFINED(0.0..0.0, R.string.notDefined);
}

enum class AttributeDifficulties(
    val nameResource: Int,
    val range: ClosedFloatingPointRange<Double>
) {
    EXTREME(R.string.extreme, 20.0..Double.MAX_VALUE),
    COMPLEX(R.string.complex, 15.0..20.0),
    NORMAL(R.string.normal, 10.0..15.0),
    SIMPLE(R.string.simple, 6.0..10.0),
    DISASTER(R.string.disaster, -Double.MAX_VALUE..6.0),
    NOT_DEFINED(R.string.notDefined, 0.0..0.0);
}






