package com.xcallibur.arget.animamastertoolkit.model

import com.xcallibur.arget.animamastertoolkit.model.Penalizer.Type.*
import com.xcallibur.arget.animamastertoolkit.utils.*
import com.xcallibur.arget.animamastertoolkit.utils.SkillDifficulties.*

class AttributeCheck {

    fun resistanceCheck(resistance: Resistances, character: Character) {
        val result = character.rollFor(resistance)
        resistanceCheck(result)

    }

    fun resistanceCheck(result: Result): Result {

        result.descriptionLong +=
            "Obtiene un resultado de: ${result.value} " +
                    "(${secondaryDifficultyReadable(result.value)})."
        return result
    }

    fun attributeCheck(characteristic: Characteristics, character: Character): Result {

        return attributeCheck(character.rollFor(characteristic))

    }


    fun attributeCheck(result: Result): Result {

        result.descriptionLong +=
            "Obtiene un resultado de: ${result.value}" +
                    "(${attributeDifficultyReadable(result.value)})."
        return result

    }


    fun skillCheck(skill: Skills, character: Character): Result {
        return skillCheck(
            skill,
            character.rollFor(skill),
            character
        )
    }


    fun skillCheck(skill: Skills, result: Result, character: Character): Result {

        var movementVelocityIndex = character.getMovementVelocityIndex()

        when (skill) {
            Skills.ATHLETICISM -> {

                if (passedDifficulty(result.value, ALMOST_IMPOSSIBLE)) movementVelocityIndex += 1
                if (passedDifficulty(result.value, IMPOSSIBLE)) movementVelocityIndex += 1
                if (passedDifficulty(result.value, ZEN)) movementVelocityIndex += 1

                val timeThatCanRun =
                    runTimeInMinutes(skillDifficultyPassed(result.value).ordinal.toDouble())
                val velocityRunning = velocityInMetersTurn(movementVelocityIndex)

                result.descriptionLong =
                    "Corriendo (gasta un punto de cansancio): " +
                            fromMinutesToReadableString(timeThatCanRun) + " a " + velocityRunning +
                            " (" + timeThatCanRun * velocityRunning + " Metros) \n"

            }
            Skills.RIDE -> {

                result.descriptionLong =
                    "Cualquier habilidad podrá usar como maximo este valor " + result.value

            }
            Skills.SWIM -> {

                movementVelocityIndex -= swimHandicapTM(result.value)

                if (movementVelocityIndex < 1) {
                    result.descriptionLong = "El personaje se hunde"

                } else {
                    result.descriptionLong =
                        "Puedes nadar con una velocidad de:" + velocityInMetersTurn(
                            movementVelocityIndex
                        ) + " m/t, Tipo de movimiento " + movementVelocityIndex
                }
            }

            Skills.JUMP -> {

                movementVelocityIndex = +jumpBonusTM(result.value)

                result.descriptionLong =
                    "Puedes saltar una distancia de: " + velocityInMetersTurn(movementVelocityIndex)

            }

            Skills.NOTICE,
            Skills.SEARCH,
            Skills.TRACK -> {
                val finalResult =
                    result.value - character.penalizer.getPenalizer(VISUAL)

                "Obtiene un resultado de: $finalResult (${secondaryDifficultyReadable(finalResult)})." +
                        " (Tirada de ${result.value} menos el penalizador visual de ${character.penalizer.getPenalizer(
                            VISUAL
                        )})"

                return result

            }

            Skills.MEDICINE -> {
                if (passedDifficulty(result.value, MODERATE)
                ) result.descriptionLong += "Establiza al objetivo, "
                if (passedDifficulty(result.value, EASY)
                ) result.descriptionLong += "Detiene la hemorragia, y "

                result.descriptionLong += "Cura un porcentaje del daño equivalente a: " +
                        "${healBonusPercentaje(result.value)} "

            }
            Skills.COMPOSURE -> {
                result.descriptionLong += "Puede añadir a sus controles de RP un bono de" +
                        "${frigidityBonusToRP(result.value)}"

            }
            Skills.FEATS_OF_STRENGTH -> {

                result.descriptionLong += "Puede añadir un bono a su fuerza de: " +
                        "${forceBonus(result.value)}. "
            }
            Skills.WITHSTAND_PAIN -> {

                val painResistanceBonus = painResistance(result.value)
                result.descriptionLong += "Puede negar penalizadores por dolor de " +
                        "$painResistanceBonus, quedando en ${character.penalizer.getPenalizer(PAIN) - painResistanceBonus}"

                character.penalizer.removePenalizer(PAIN, painResistanceBonus)

            }

            else -> {
            }
        }

        result.descriptionLong +=
            "Obtiene un resultado de: ${result.value} " +
                    "(${secondaryDifficultyReadable(result.value)})."
        return result
    }

}