package com.xcallibur.arget.animamastertoolkit.model

import com.xcallibur.arget.animamastertoolkit.model.Roll.RollTypes.*
import com.xcallibur.arget.animamastertoolkit.utils.SkillDifficulties
import kotlin.random.Random

/*
Controles de Habilidad
Son los controles que se realizan lanzado un D100 y sumando el
resultado a la habilidad del personaje. Hay dos tipos distintos: Los
controles de Dificultad son aquellos en los que se debe
superar una cantidad fijada previamente por el Director
de Juego, mientras que en los controles enfrentados
se comparan directamente dos habilidades entre sí. Estos
controles son muy diversos y especializados, por lo que se
explicarán con detalle en sus respectivas secciones.
 */


/*
Controles de Resistencia
Los controles de Resistencia representan la capacidad de
un personaje de evitar consecuencias perjudiciales para
él. Existen cinco Resistencias distintas dependiendo del
tipo de efecto que ataque al sujeto. Para pasar un control
de Resistencia, el personaje tiene simplemente que lanzar
un D100 y sumar el resultado a la Resistencia que pone a
prueba. Si supera la dificultad requerida, ha conseguido
evitar el efecto. No importa lo elevada que sea la dificultad
de una Resistencia, un resultado de 100 representa siempre
que ha conseguido evitar con éxito el efecto. Al contrario de
lo que dicta la regla general, no se admite la Tirada Abierta en
los controles de Resistencia.
Si un personaje tiene una Resistencia 20 puntos por encima
de la dificultad que pone a prueba, no necesita tirar los dados: la
supera automáticamente.
 */


/*
Controles de características
Estos controles son chequeos que se realizan directamente sobre las
características. Para efectuarlos se lanza un único D10 se suma el resultado al del
atributo que se pone a prueba; si el valor iguala o supera la dificultad a superar,
el personaje habrá tenido éxito en su control, y la diferencia por encima o por
debajo de su valor es el nivel de éxito o de fracaso. Naturalmente, cuanto más
elevada sea una característica, mayores posibilidades hay de conseguir el éxito.
Por lo general, el Director de Juego es quien debe determinar cuan elevada
es la dificultad del control, usando como referencia los valores de la Tabla 1.
Si en algún momento una regla pide que un personaje haga un control de una
característica sin precisar su valor, se considera automáticamente que el control
tiene dificultad normal, o lo que es lo mismo, de 10.
 */


class Roll {

    var maximunValue = SkillDifficulties.IMPOSSIBLE.range.start
    var failureValue = 3.0
    var criticalValue = 90.0
    var rollType = D100

    var d10Rule = false
    var critWithPalindrome = false


    constructor()
    constructor(rollType: RollTypes) {
        this.rollType = rollType

        if (rollType == D10) {
            maximunValue = 20.0
            failureValue = -1.0
            criticalValue = 99.0
            d10Rule = true
        }


    }

    constructor(
        maximunValue: Double,
        failureValue: Double,
        criticalValue: Double,
        rollType: RollTypes,
        d10Rule: Boolean,
        critWithPalindrome: Boolean
    ) {
        this.maximunValue = maximunValue
        this.failureValue = failureValue
        this.criticalValue = criticalValue
        this.rollType = rollType
        this.d10Rule = d10Rule
        this.critWithPalindrome = critWithPalindrome
    }


    fun simpleRoll100(): Double {
        return (Random.nextFloat() * 100).toDouble()
    }

    fun simpleRoll10(): Double {
        return (Random.nextFloat() * 10).toDouble()
    }

    enum class RollTypes {
        D100,
        D10
    }

    fun simpleRoll(rollType: RollTypes): Double {
        when (rollType) {
            D100 -> simpleRoll100()

            D10 -> simpleRoll10()
        }
        return simpleRoll100()
    }


    fun roll(baseValue: Double, with100Pass: Boolean): Result {

        val roll = simpleRoll(rollType)

        return roll(baseValue, roll, with100Pass)
    }


    fun roll(baseValue: Double, roll: Double, with100Pass: Boolean): Result {

        var result = Result("", 0.0)
        result.value = roll + baseValue
        result.descriptionRoll += "Roll: $roll"

        if (with100Pass) {
            if (roll > 99.0) {
                result.descriptionRoll += " Critical Roll! Succeeding automatically"
                result.value = 999.0

                return result
            }
        }

        if (d10Rule) {
            if (roll > 9.0) {
                result.value += roll + 2.0
                result.descriptionRoll += " Critical roll! Adding 2 to the result"
                return result
            }
        }

        if (roll <= failureValue) {
            //FAILURE

            val rollFailure = simpleRoll(rollType)
            result.descriptionRoll += " Failure roll! (Failure roll: $rollFailure)"
            result.value = baseValue - rollFailure

            return result

        }

        if (roll >= criticalValue || checkIfPalindromeCritical(roll)) {
            //CRITICAL
            result.descriptionRoll += " Critical roll! ("
            result = criticalCalculation(1, result)

        }

        if (result.value > maximunValue) {
            result.descriptionRoll += (" Maximun value reached (Maximun value: $maximunValue)")
            result.value = maximunValue
            return result

        }

        return result


    }

    private fun criticalCalculation(numberOfRoll: Int, result: Result): Result {

        val roll = simpleRoll(rollType)

        if (roll > criticalValue + numberOfRoll || checkIfPalindromeCritical(roll)) {
            result.value += roll
            result.descriptionRoll += "Roll $numberOfRoll: $roll, "
            return criticalCalculation(numberOfRoll + 1, result)

        } else {
            result.value += roll
            result.descriptionRoll += " Roll $numberOfRoll: $roll)"
            return result

        }
    }

    private fun checkIfPalindromeCritical(value: Double): Boolean {

        if (value.toString()[0] == value.toString()[1] && critWithPalindrome)
            return true

        return false

    }


}