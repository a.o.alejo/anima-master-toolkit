package com.xcallibur.arget.animamastertoolkit.utils

import com.xcallibur.arget.animamastertoolkit.App


fun passedDifficulty(result: Double, difficulty: SkillDifficulties): Boolean {

    if (result > difficulty.range.start) return true
    return false

}


fun passedDifficulty(result: Double, difficulty: AttributeDifficulties): Boolean {

    if (result > difficulty.range.start) return true
    return false

}


fun skillDifficultyPassed(result: Double): SkillDifficulties {

    SkillDifficulties.values().forEach {
        if (it.range.contains(result)) return it
    }

    return SkillDifficulties.NOT_DEFINED

}

fun secondaryDifficultyReadable(result: Double): String {

    App.getResourses().getString(skillDifficultyPassed(result).nameResource)

    return ""

}


fun attributeDifficultyPassed(result: Double): AttributeDifficulties {

    AttributeDifficulties.values().forEach {
        if (it.range.contains(result)) return it
    }

    return AttributeDifficulties.NOT_DEFINED

}

fun attributeDifficultyReadable(result: Double): String {

    App.getResourses().getString(attributeDifficultyPassed(result).nameResource)

    return ""
}

