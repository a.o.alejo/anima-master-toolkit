package com.xcallibur.arget.animamastertoolkit.model

import com.xcallibur.arget.animamastertoolkit.dataModel.Consumable
import com.xcallibur.arget.animamastertoolkit.utils.DamageTypes
import com.xcallibur.arget.animamastertoolkit.utils.Locations
import com.xcallibur.arget.animamastertoolkit.utils.Zones

class Armour {

    val requeriment = 10.0
    val penalizer = Penalizer()

    var fortitude = 10.0
    var prescence = 10.0

    var active = false
    var zonesProtected = arrayListOf<Zones>()
    var armourType = List(DamageTypes.values().size) { Consumable() }

    fun getTAforLocation(
        checkIfActive: Boolean,
        location: Locations,
        damageType: DamageTypes
    ): Double {
        if (checkIfActive) {
            if (!active)
                return 0.0
        }

        if (zonesProtected.contains(location.zone)) {
            return armourType[damageType.ordinal].temporaryValue
        }

        return 0.0
    }


}