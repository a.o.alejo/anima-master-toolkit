package com.xcallibur.arget.animamastertoolkit.utils

import kotlin.math.*

fun runTimeInMinutes(result: Double): Double {

    //El tiempo total que puede correr gastando un punto de cansancio, respecto al resultado de una tirada, expresado en minutos
    val y: Double = -1.776699930465349 + result * (0.07765266618198406 +
            result * (-0.0003752393167574659 + result * 6.904522596922215E-07))
    return exp(y)


}

fun velocityInMetersTurn(movementType: Double): Double {

    //La velocidad que puede desplazarse un personaje, en base a su tipo de movimiento. Expresado en Metros cada turno

    when (movementType) {

        in -Double.MAX_VALUE..3.0 -> {
            return 1.223054513601562 * movementType.pow(1.709511291351458)
        }

        in 3.0..13.0 -> {
            return 36.21958041049630 + movementType * (-60.12124227834859 +
                    movementType * (34.17718108356527 + movementType * (-8.314210543723959 +
                    movementType * (1.027576671629701 + movementType * (-0.06314856711579641 +
                    movementType * 0.001535947712348405)))))
        }

        in 13.0..22.0 -> {
            val aux = movementType * movementType
            return -377671.6146232519 + aux * (8577.848761961321 +
                    aux * (-77.23883101192683 + aux * (0.3447921182313396 +
                    aux * (-0.0007632964056416090 + aux * 6.710665750314517E-07))))

        }

    }

    return -1.0

}


fun swimHandicapTM(result: Double): Double {

    //La cantidad de TM que quita el nadar, en base al resultado

    val x2: Double = result * result * ln(result)
    val x3: Double = result * result * result
    val x4: Double = 1.0 / (result * sqrt(result))
    return -3.568615436196096 + 0.001307514293719292 * result + 9.688828945699478E-06 * x2 - 8.616337267340313E-08 * x3 - 399.3834138467336 * x4


}


fun jumpBonusTM(result: Double): Double {

    //La cantidad de TM que añade el saltar, en base al resultado

    val x1: Double = result * result * sqrt(result)
    val x2: Double = result * result * result
    val x3: Double = ln(result)
    return -2.242876793439440 + 7.070897435558131E-06 * x1 - 2.742890336358409E-07 * x2 + 0.4904138260812070 * x3

}


fun healBonusPercentaje(result: Double): Double {

    //El porcentaje de la herida que cura

    val y: Double
    val x1: Double = result * result * sqrt(result)
    val x2: Double = result * result * result
    val x3: Double = 1.0 / result
    return 5.527144611762712 + 0.0001046405074018779 * x1 - 3.985500631573723E-06 * x2 - 269.0382923984692 * x3

}


fun frigidityBonusToRP(result: Double): Double {

    //El bono que se añade a las resistencias psiquicas

    val x1: Double = result * result * sqrt(result)
    val x2: Double = result * result * result
    val x3: Double = 1.0 / result
    return 5.527144611762712 + 0.0001046405074018779 * x1 - 3.985500631573723E-06 * x2 - 269.0382923984692 * x3

}

fun forceBonus(result: Double): Double {

    return (-0.001845145524532049 + result * (-0.003561805534184406 + result * 0.0001409863705555805)) / (1.0 + result * (0.007339821550119458 + result * -6.741219571366000E-06))

}


fun painResistance(result: Double): Double {

    val x2: Double = 1.0 / sqrt(result)
    val x3: Double = ln(result) / result
    val x4: Double = 1.0 / result
    return (-649.5475790449175 + 0.1936820300513185 * result + 19003.85043188751 * x2 - 38547.03182007413 * x3
            + 48877.94977434309 * x4)

}

class Point(val x: Double, val y: Double)

fun extraLimit(pointA: Point, pointB: Point, value: Double): Double {

    var slope = (pointB.y - pointA.y) / (pointB.x - pointA.x)

    return if (value < pointA.x)
        -((slope * (pointA.x - value)) - pointA.y)
    else
        -((slope * (pointB.x - value)) - pointB.y)

}







