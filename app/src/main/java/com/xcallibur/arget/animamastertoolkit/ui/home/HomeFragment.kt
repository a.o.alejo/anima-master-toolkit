package com.xcallibur.arget.animamastertoolkit.ui.home

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.xcallibur.arget.animamastertoolkit.databinding.FragmentHomeBinding
import com.xcallibur.arget.animamastertoolkit.utils.runTimeInMinutes


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentHomeBinding.inflate(this.layoutInflater, container, false)
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)

        createChart()

        return binding.root
    }

    private fun createChart() {

        val desc = Description()
        desc.text =
            "El tiempo total que puede correr gastando un punto de cansancio, respecto al resultado de una tirada, expresado en minutos"
        binding.chart.description = desc
        binding.chart.setTouchEnabled(true)

        val xAxis: XAxis = binding.chart.xAxis
        xAxis.textSize = 11f
        xAxis.textColor = Color.RED
        xAxis.setDrawGridLines(true)
        xAxis.setDrawAxisLine(true)

        val leftAxis: YAxis = binding.chart.axisLeft
        leftAxis.textColor = ColorTemplate.getHoloBlue()
        leftAxis.axisMaximum = 200f
        leftAxis.axisMinimum = 0f
        leftAxis.setDrawGridLines(true)
        leftAxis.isGranularityEnabled = true

        val values1 = arrayListOf<Entry>()

        for (i in -20..450 step 5) {

            values1.add(Entry(i.toFloat(), runTimeInMinutes(i.toDouble()).toFloat()))
        }

        var set1 = LineDataSet(values1, "Atletismo")
        set1.values = values1

        set1.axisDependency = YAxis.AxisDependency.LEFT
        set1.color = ColorTemplate.getHoloBlue()
        set1.lineWidth = 2f
        set1.fillColor = ColorTemplate.getHoloBlue()
        set1.highLightColor = Color.rgb(244, 117, 117)
        set1.setDrawCircleHole(false)

        val data = LineData(set1)
        data.setValueTextColor(Color.BLACK)
        data.setValueTextSize(9f)

        binding.chart.data = data

    }


}