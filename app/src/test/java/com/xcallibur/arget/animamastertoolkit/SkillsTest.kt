package com.xcallibur.arget.animamastertoolkit

import com.xcallibur.arget.animamastertoolkit.model.*
import com.xcallibur.arget.animamastertoolkit.utils.Skills
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class SkillsTest {


    @Test
    fun atletismSkillTest10() {

        val character = Character()

        val result = character.skillCheck(Skills.ATLETISMO, Result("Base value of 10", 10.0))

        assertEquals("", result.toString())
    }


}
