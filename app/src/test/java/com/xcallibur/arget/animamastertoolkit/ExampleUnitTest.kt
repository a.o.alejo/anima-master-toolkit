package com.xcallibur.arget.animamastertoolkit

import com.xcallibur.arget.animamastertoolkit.dataModel.Attribute
import com.xcallibur.arget.animamastertoolkit.model.*
import com.xcallibur.arget.animamastertoolkit.utils.*
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    val tableModifiers = arrayListOf(
        -100.0,
        -30.0,
        -20.0,
        -10.0,
        -5.0,
        0.0,
        5.0,
        5.0,
        10.0,
        10.0,
        15.0,
        20.0,
        20.0,
        25.0,
        25.0,
        30.0,
        35.0,
        35.0,
        40.0,
        40.0,
        45.0
    )

    @Test
    fun rollQuantityPromTest() {
        var value = 0.0
        val roll = Roll()

        for (i in 1..10000) {
            value += roll.simpleRoll100()
        }

        assertEquals(50.0, value / 10000, 1.0)

    }


    @Test
    fun checkNewAttributesNew() {
        val attribute =
            Attribute(
                5.0,
                Attribute.AttributeTypes.CHARACTERISTIC
            )
        val values = arrayListOf<Double>()

        for (i in 0..20) {
            attribute.baseValue = i.toDouble()
            values.add(tableModifiers[i] - attribute.calculateModifier())
        }

        assertEquals("[]", values.filter { it >= 2.6 }.toString())

    }

    @Test
    fun checkNewAttributesOld() {
        val attribute =
            Attribute(
                5.0,
                Attribute.AttributeTypes.CHARACTERISTIC
            )
        val values = arrayListOf<Double>()

        for (i in 0..20) {
            attribute.baseValue = i.toDouble()
            values.add(attribute.calculateModifierOld())
        }

        assertEquals(tableModifiers.toString(), values.toString())

    }


    @Test
    fun clearNegativeTemporaryAttributes() {
        val character = Character()

        character.characteristics[0].temporaryValue = 5.0
        character.characteristics[1].temporaryValue = -5.0

        character.clearNegativeStatus()

        assertEquals("6.0", character.characteristics[1].total().toString())

    }

    @Test
    fun clearPositiveTemporaryAttributes() {
        val character = Character()

        character.characteristics[0].temporaryValue = 5.0
        character.characteristics[1].temporaryValue = -5.0

        character.clearPositiveStatus()

        assertEquals("6.0", character.characteristics[0].total().toString())

    }

    @Test
    fun clearTemporaryAttributes() {
        val character = Character()

        character.characteristics[0].temporaryValue = 5.0
        character.characteristics[1].temporaryValue = -5.0

        character.clearStatus()

        assertEquals(Character().characteristics.toString(), character.characteristics.toString())

    }

    @Test
    fun acrobaticsCheck() {
        val character = Character()

        val result = character.skillCheck(Skills.ACROBACIAS, Result("", 50.0))

        assertEquals(true, result.toString().contains("Obtiene un resultado de:"))

    }

    @Test
    fun difficultiesEasy() {
        val roll = SkillRoll()

        val result =
            secondaryDifficultyReadable(40.0)

        assertEquals("Fácil", result)

    }

    @Test
    fun difficultiesNegative() {
        val roll = SkillRoll()

        val result =
            secondaryDifficultyReadable(-100.0)

        assertEquals("Desastrozo", result)

    }

    @Test
    fun difficultiesExtraValue() {
        val roll = SkillRoll()

        val result =
            secondaryDifficultyReadable(
                10000.0
            )

        assertEquals("Zen", result)

    }


    @Test
    fun velocityValue5() {

        assertEquals(20.0,
            velocityInMetersTurn(5.0), 1.0)

    }

    @Test
    fun velocityValue15() {

        assertEquals(250.0,
            velocityInMetersTurn(15.0), 2.0)

    }

    @Test
    fun velocityValue10() {

        assertEquals(35.0,
            velocityInMetersTurn(10.0), 1.0)

    }

    @Test
    fun runTime40() {

        assertEquals(2.0,
            runTimeInMinutes(40.0), 0.50)

    }

    @Test
    fun runTime140() {

        assertEquals(40.0,
            runTimeInMinutes(140.0), 4.00)

    }

    @Test
    fun runTime280() {

        assertEquals(300.0,
            runTimeInMinutes(280.0), 0.50)

    }

    @Test
    fun minuteReadable1548() {

        assertEquals("1 Days, 1 Hours, 48 Minutes, 11 Turns", fromMinutesToReadableString(1548.58))

    }

    @Test
    fun minuteReadable15488741() {

        assertEquals(
            "21 Months, 28 Days, 19 Hours, 15 Minutes, 11 Turns",
            fromMinutesToReadableString(948675.58781)
        )

    }


}
